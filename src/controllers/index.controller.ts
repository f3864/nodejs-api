import { Request, Response } from "express";

const indexController = (req: Request, res: Response) => {
  console.log("Je suis le controller");
  res.send("Hello world");
};
export default indexController;
