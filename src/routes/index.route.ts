import express from "express";
import indexController from "@controllers/index.controller";
import indexMiddleware from "@middlewares/index.middleware";
const router = express.Router();

router.use("/", indexMiddleware, indexController);

export default router;
