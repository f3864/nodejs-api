import express from "express";
import router from "@routes/index.route";
const app = express();

// routes
app.use(router);

// fonction qui va exécuter notre serveur
const main = async () => {
  try {
    app.listen(3000);
    console.log("Serveur lancé sur le port 3000");
  } catch (error) {
    console.log("error: ", error);
  }
};

main();
