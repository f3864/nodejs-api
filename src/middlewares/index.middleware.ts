import { NextFunction, Request, Response } from "express";

const indexMiddleware = (req: Request, res: Response, next: NextFunction) => {
  console.log("Je suis un middleware");

  // si on ne met pas le next(), on ne peut pas passer au middleware suivant (index.controller) et donc le serveur attend sans renvoyer de réponse
  next();
};
export default indexMiddleware;
